# Changelog

## [0.2.1] - 2024-05-26
### Fixed
- CI fixed for publish package

## [0.2.0] - 2024-05-25

### Added
- CI run linters before compile code 
- restructure plugin folder
- added function for transform point from one crs to another
- added tests

## [0.1.0] - 2024-05-05

### Added

- functions for working with s2 index
- functions for working with h3 index

