.PHONY: format_code format_rust_code, format_python_code

format_code: format_python_code format_rust_code

lint_code: linters_python linters_rust

test_code: test_python_code

linters_python:
	ruff check .

linters_rust:
	cargo clippy

format_python_code:
	black .
	isort .

format_rust_code:
	cargo fmt

test_python_code:
	pytest

