# polars-geo-tools

## Overview 

This polars plugin provides functionality for working with geo data.
Now time plugin support for working with [s2_index](http://s2geometry.io/about/overview) and [h3_index](https://h3geo.org/docs/highlights/indexing/). In nearest future plugin will be support functionality of [geopandas](https://geopandas.org/en/stable/)

## Getting Started

### Installation

```bash
pip install polars-geo-tools
```

### Example of usage

