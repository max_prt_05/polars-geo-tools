import polars as pl
from pyogrio.raw import read_arrow

from polars_geo_tools import area

metadata, table = read_arrow("Russia_regions.geojson")
df = pl.from_arrow(table)

print(
    df.select(
        "region",
        area("wkb_geometry").alias("area"),
    )
)
