use geo::BoundingRect;
use geo_types::{Geometry, Rect};

pub struct GeomBounds {
    pub min_x: f64,
    pub min_y: f64,
    pub max_x: f64,
    pub max_y: f64,
}

impl GeomBounds {
    fn from_bounding_rect(bounding_rect: &Rect) -> Self {
        GeomBounds {
            min_x: bounding_rect.min().x,
            min_y: bounding_rect.min().y,
            max_x: bounding_rect.max().x,
            max_y: bounding_rect.max().y,
        }
    }
}

pub fn geom_bounds(geom: &Geometry) -> Option<GeomBounds> {
    match geom {
        Geometry::Point(p) => {
            let bounding_rect = p.bounding_rect();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::Line(l) => {
            let bounding_rect = l.bounding_rect();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::LineString(l) => {
            let bounding_rect = l.bounding_rect().unwrap();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::Polygon(p) => {
            let bounding_rect = p.bounding_rect().unwrap();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::MultiPoint(mp) => {
            let bounding_rect = mp.bounding_rect().unwrap();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::MultiPolygon(mp) => {
            let bounding_rect = mp.bounding_rect().unwrap();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        Geometry::MultiLineString(l) => {
            let bounding_rect = l.bounding_rect().unwrap();
            Some(GeomBounds::from_bounding_rect(&bounding_rect))
        }
        _ => None,
    }
}
// }

#[cfg(test)]
mod bounds_functions_test {
    use super::*;
    use geo::{line_string, point};

    #[test]
    fn correct_bound_point() {
        let point = point! {
            x: 0.0f64, y: 1.0f64
        };
        let geom = Geometry::Point(point);
        let geom_bounds = geom_bounds(&geom);
        assert!(geom_bounds.is_some());
        let geom_bounds = geom_bounds.unwrap();
        assert_eq!(geom_bounds.min_x, 0.0f64);
        assert_eq!(geom_bounds.min_y, 1.0f64);
        assert_eq!(geom_bounds.max_x, 0.0f64);
        assert_eq!(geom_bounds.max_y, 1.0f64);
    }

    #[test]
    fn correct_bound_linestring() {
        let line_string = line_string![
            (x: 40.02f64, y: 116.34f64),
            (x: 42.02f64, y: 116.34f64),
            (x: 42.02f64, y: 118.34f64),
        ];
        let geom = Geometry::LineString(line_string);
        let geom_bounds = geom_bounds(&geom);
        assert!(geom_bounds.is_some());
        let geom_bounds = geom_bounds.unwrap();
        assert_eq!(geom_bounds.min_x, 40.02f64);
        assert_eq!(geom_bounds.min_y, 116.34f64);
        assert_eq!(geom_bounds.max_x, 42.02f64);
        assert_eq!(geom_bounds.max_y, 118.34f64);
    }
}
