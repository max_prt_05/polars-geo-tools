#![allow(clippy::unused_unit)]
use crate::bounds_functions::geom_bounds;
use geo::Area;
use geo_types::Geometry;
use polars::datatypes::DataType;
use polars::prelude::*;
use pyo3_polars::derive::polars_expr;
use wkb::wkb_to_geom;

#[polars_expr(output_type=Float64)]
fn area(inputs: &[Series]) -> PolarsResult<Series> {
    let binary_arr: &ChunkedArray<BinaryType> = inputs[0].binary()?;
    let res: Vec<Option<f64>> = binary_arr
        .into_iter()
        .map(|item| match item {
            Some(item) => {
                let wkb_geom = item.to_vec();
                let geom = wkb_to_geom(&mut wkb_geom.as_slice()).unwrap();
                match geom {
                    Geometry::Point(p) => Some(p.unsigned_area()),
                    Geometry::Line(l) => Some(l.unsigned_area()),
                    Geometry::LineString(ls) => Some(ls.unsigned_area()),
                    Geometry::Polygon(p) => Some(p.unsigned_area()),
                    Geometry::MultiPoint(mp) => Some(mp.unsigned_area()),
                    Geometry::MultiLineString(mls) => Some(mls.unsigned_area()),
                    Geometry::MultiPolygon(mp) => Some(mp.unsigned_area()),
                    _ => Some(0.0),
                }
            }
            _ => None,
        })
        .collect();
    Ok(Float64Chunked::new("res", res).into_series())
}

#[polars_expr(output_type=String)]
fn geom_type(inputs: &[Series]) -> PolarsResult<Series> {
    let binary_arr = inputs[0].binary()?;
    let res: Vec<Option<&str>> = binary_arr
        .into_iter()
        .map(|item| match item {
            Some(item) => {
                let wkb_geom = item.to_vec();
                let geom = wkb_to_geom(&mut wkb_geom.as_slice()).unwrap();
                match geom {
                    Geometry::Point(_) => Some("Point"),
                    Geometry::Line(_) => Some("Line"),
                    Geometry::LineString(_) => Some("LineString"),
                    Geometry::Polygon(_) => Some("Polygon"),
                    Geometry::MultiPoint(_) => Some("MultiPoint"),
                    Geometry::MultiLineString(_) => Some("MultiLineString"),
                    Geometry::MultiPolygon(_) => Some("MultiPolygon"),
                    _ => Some("Unknown"),
                }
            }
            None => None,
        })
        .collect();
    Ok(StringChunked::new("res", res).into_series())
}

fn geom_bounds_output_type(_: &[Field]) -> PolarsResult<Field> {
    let v = vec![
        Field::new("minx", DataType::Float64),
        Field::new("miny", DataType::Float64),
        Field::new("maxx", DataType::Float64),
        Field::new("maxy", DataType::Float64),
    ];
    Ok(Field::new("bounds", DataType::Struct(v)))
}

#[polars_expr(output_type_func=geom_bounds_output_type)]
fn bounds(inputs: &[Series]) -> PolarsResult<Series> {
    let binary_arr = inputs[0].binary()?;
    let mut min_x_bound_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("minx", binary_arr.len());
    let mut min_y_bound_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("miny", binary_arr.len());
    let mut max_x_bound_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("maxx", binary_arr.len());
    let mut max_y_bound_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("maxy", binary_arr.len());

    for wkb_geom in binary_arr.into_iter() {
        match wkb_geom {
            Some(wkb_geom) => {
                let wkb_geom = wkb_geom.to_vec();
                let geom = wkb_to_geom(&mut wkb_geom.as_slice()).unwrap();
                let geom_bounds = geom_bounds(&geom);
                match geom_bounds {
                    Some(geom_bounds) => {
                        min_x_bound_builder.append_value(geom_bounds.min_x);
                        min_y_bound_builder.append_value(geom_bounds.min_y);
                        max_x_bound_builder.append_value(geom_bounds.max_x);
                        max_y_bound_builder.append_value(geom_bounds.max_y);
                    }
                    None => {
                        min_x_bound_builder.append_null();
                        min_y_bound_builder.append_null();
                        max_x_bound_builder.append_null();
                        max_y_bound_builder.append_null();
                    }
                }
            }
            None => {
                min_x_bound_builder.append_null();
                min_y_bound_builder.append_null();
                max_x_bound_builder.append_null();
                max_y_bound_builder.append_null();
            }
        }
    }

    let out_chunked = StructChunked::new(
        "bounds",
        &[
            min_x_bound_builder.finish().into_series(),
            min_y_bound_builder.finish().into_series(),
            max_x_bound_builder.finish().into_series(),
            max_y_bound_builder.finish().into_series(),
        ],
    )?;
    Ok(out_chunked.into_series())
}
