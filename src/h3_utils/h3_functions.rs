use h3o::{CellIndex, LatLng, Resolution};

fn resolution_from_u64(resolution: u64) -> Resolution {
    match resolution {
        1 => Resolution::One,
        2 => Resolution::Two,
        3 => Resolution::Three,
        4 => Resolution::Four,
        5 => Resolution::Five,
        6 => Resolution::Six,
        7 => Resolution::Seven,
        8 => Resolution::Eight,
        9 => Resolution::Nine,
        10 => Resolution::Ten,
        11 => Resolution::Eleven,
        12 => Resolution::Twelve,
        13 => Resolution::Thirteen,
        14 => Resolution::Fourteen,
        15 => Resolution::Fifteen,
        _ => {
            panic!("H3 index system support resolution from 1 to 15 included");
        }
    }
}

pub fn h3_lat_lon_to_cell_one_point(lat: f64, lon: f64, resolution: u64) -> Option<u64> {
    let lat_lon = LatLng::new(lat, lon);

    match lat_lon {
        Ok(lat_lon) => {
            let cell_id = lat_lon.to_cell(resolution_from_u64(resolution));
            Some(u64::from(cell_id))
        }
        Err(_) => None,
    }
}

pub fn h3_cell_id_to_lat_lon_one_point(cell_id: u64) -> Option<(f64, f64)> {
    let cell_id = CellIndex::try_from(cell_id);
    match cell_id {
        Ok(cell_id) => {
            let lat_lng = LatLng::from(cell_id);
            Some((lat_lng.lat(), lat_lng.lng()))
        }
        _ => None,
    }
}

#[cfg(test)]
mod h3_tests {
    use super::*;
    use approx::assert_relative_eq;

    #[test]
    fn correct_get_h3_index() {
        let cell_id_opt = h3_lat_lon_to_cell_one_point(55.96197598047027, 37.35753533375114, 12);
        assert!(cell_id_opt.is_some());
        let cell_id = cell_id_opt.unwrap();
        assert_eq!(cell_id, 630814701394381311u64);
    }

    #[test]
    fn correct_get_lat_lon_from_h3_index() {
        let lat_lon_opt = h3_cell_id_to_lat_lon_one_point(630814701394381311u64);
        assert!(lat_lon_opt.is_some());
        let lat_lon = lat_lon_opt.unwrap();
        assert_relative_eq!(lat_lon.0, 55.96200512120371, epsilon = f64::EPSILON);
        assert_relative_eq!(lat_lon.1, 37.35748872157142, epsilon = f64::EPSILON);
    }
}
