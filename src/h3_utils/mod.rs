#![allow(clippy::unused_unit)]

use polars::prelude::*;
use pyo3_polars::derive::polars_expr;
use serde::Deserialize;

mod h3_functions;
use h3_functions::{h3_cell_id_to_lat_lon_one_point, h3_lat_lon_to_cell_one_point};

#[derive(Deserialize)]
struct H3Kwargs {
    resolution: u64,
}

#[polars_expr(output_type=UInt64)]
fn h3_lat_lon_to_cell_id(inputs: &[Series], kwargs: H3Kwargs) -> PolarsResult<Series> {
    let lat_lon_ca = inputs[0].struct_()?;

    let lat_ser = &lat_lon_ca.fields()[0];
    let lon_ser = &lat_lon_ca.fields()[1];

    let lat_ser = match lat_ser.dtype() {
        DataType::Float32 => lat_ser.cast(&DataType::Float64)?,
        DataType::Float64 => lat_ser.clone(),
        _ => polars_bail!(InvalidOperation:"Expected lat and lon get type float32 or float64"),
    };

    let lon_ser: Series = match lon_ser.dtype() {
        DataType::Float32 => lon_ser.cast(&DataType::Float64)?,
        DataType::Float64 => lon_ser.clone(),
        _ => polars_bail!(InvalidOperation:"Expected lat and lon get type float32 or float64"),
    };

    let lat_ca = lat_ser.f64()?;
    let lon_ca = lon_ser.f64()?;

    let out_ca: ChunkedArray<UInt64Type> = lat_ca
        .into_iter()
        .zip(lon_ca.into_iter())
        .map(|(lat_opt, lon_opt)| match (lat_opt, lon_opt) {
            (Some(lat), Some(lon)) => h3_lat_lon_to_cell_one_point(lat, lon, kwargs.resolution),
            _ => None,
        })
        .collect_ca("cell_id");

    Ok(out_ca.into_series())
}

fn h3_cell_id_to_lat_lon_output_type_func(_: &[Field]) -> PolarsResult<Field> {
    let v = vec![
        Field::new("lat", DataType::Float64),
        Field::new("lon", DataType::Float64),
    ];
    Ok(Field::new("coords", DataType::Struct(v)))
}

#[polars_expr(output_type_func=h3_cell_id_to_lat_lon_output_type_func)]
fn h3_cell_id_to_lat_lon(inputs: &[Series]) -> PolarsResult<Series> {
    let cell_id_ca = inputs[0].u64()?;

    let mut lat_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("lat", cell_id_ca.len());
    let mut lon_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("lon", cell_id_ca.len());

    cell_id_ca
        .into_iter()
        .for_each(|cell_id_opt| match cell_id_opt {
            Some(cell_id) => {
                let lat_lon = h3_cell_id_to_lat_lon_one_point(cell_id);
                match lat_lon {
                    Some((lat, lon)) => {
                        lat_builder.append_value(lat);
                        lon_builder.append_value(lon);
                    }
                    _ => {
                        lat_builder.append_null();
                        lon_builder.append_null();
                    }
                }
            }
            None => {
                lat_builder.append_null();
                lon_builder.append_null();
            }
        });

    let out_ca = StructChunked::new(
        "coords",
        &[
            lat_builder.finish().into_series(),
            lon_builder.finish().into_series(),
        ],
    )?;

    Ok(out_ca.into_series())
}
