mod expressions;
mod utils;

#[cfg(target_os = "linux")]
use jemallocator::Jemalloc;

#[global_allocator]
#[cfg(target_os = "linux")]
static ALLOC: Jemalloc = Jemalloc;

use pyo3::types::PyModule;
use pyo3::{pymodule, Bound, PyResult, Python};
mod bounds_functions;
mod h3_utils;
mod s2_utils;
mod transform_utils;

#[pymodule]
fn polars_geo_tools(_py: Python, _m: &Bound<'_, PyModule>) -> PyResult<()> {
    Ok(())
}
