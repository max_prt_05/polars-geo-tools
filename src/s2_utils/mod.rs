#![allow(clippy::unused_unit)]

use polars::prelude::*;
use pyo3_polars::derive::polars_expr;
use serde::Deserialize;

mod s2_functions;
use crate::s2_utils::s2_functions::*;

#[derive(Deserialize)]
struct S2Kwargs {
    level: u64,
}

#[polars_expr(output_type=UInt64)]
fn s2_lat_lon_to_cell_id(inputs: &[Series], kwargs: S2Kwargs) -> PolarsResult<Series> {
    let lat_lon_ca = inputs[0].struct_()?;

    let lat_ser = lat_lon_ca.field_by_name("lat")?;
    let lon_ser = lat_lon_ca.field_by_name("lon")?;

    let lat_ser = match lat_ser.dtype() {
        DataType::Float32 => lat_ser.cast(&DataType::Float64)?,
        DataType::Float64 => lat_ser,
        _ => polars_bail!(InvalidOperation:"Expected lat and lon get type float32 or float64"),
    };

    let lon_ser = match lon_ser.dtype() {
        DataType::Float32 => lon_ser.cast(&DataType::Float64)?,
        DataType::Float64 => lon_ser,
        _ => polars_bail!(InvalidOperation:"Expected lat and lon get type float32 or float64"),
    };

    let lat_ca = lat_ser.f64()?;
    let lon_ca = lon_ser.f64()?;

    let cell_id_ca: ChunkedArray<UInt64Type> = lat_ca
        .into_iter()
        .zip(lon_ca.into_iter())
        .map(|(lat_opt, lon_opt)| match (lat_opt, lon_opt) {
            (Some(lat), Some(lon)) => Some(s2_lat_lon_to_cell_id_one_point(lat, lon, kwargs.level)),
            _ => None,
        })
        .collect_ca("s2_cell_id");

    Ok(cell_id_ca.into_series())
}

fn s2_cell_id_to_lat_lon_output_type_func(_: &[Field]) -> PolarsResult<Field> {
    let v = vec![
        Field::new("lat", DataType::Float64),
        Field::new("lon", DataType::Float64),
    ];
    Ok(Field::new("coords", DataType::Struct(v)))
}

#[polars_expr(output_type_func=s2_cell_id_to_lat_lon_output_type_func)]
fn s2_cell_id_to_lat_lon(inputs: &[Series]) -> PolarsResult<Series> {
    let cell_id_ca = inputs[0].u64()?;

    let mut lat_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("lat", cell_id_ca.len());
    let mut lon_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new("lon", cell_id_ca.len());

    cell_id_ca
        .into_iter()
        .for_each(|cell_id_opt| match cell_id_opt {
            Some(cell_id) => {
                let (lat, lon) = s2_cell_id_to_lat_lon_one_point(cell_id);
                lat_builder.append_value(lat);
                lon_builder.append_value(lon);
            }
            None => {
                lat_builder.append_null();
                lon_builder.append_null();
            }
        });

    let out_ca = StructChunked::new(
        "coords",
        &[
            lat_builder.finish().into_series(),
            lon_builder.finish().into_series(),
        ],
    )?;

    Ok(out_ca.into_series())
}
