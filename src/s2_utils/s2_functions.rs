#![allow(dead_code)]

use std::collections::HashSet;

use s2::{cellid::CellID, latlng::LatLng};

pub fn s2_lat_lon_to_cell_id_one_point(lat: f64, lon: f64, level: u64) -> u64 {
    let lat_lng = LatLng::from_degrees(lat, lon);
    let cell_id = CellID::from(lat_lng);
    cell_id.parent(level).0
}

pub fn s2_cell_id_to_lat_lon_one_point(cell_id: u64) -> (f64, f64) {
    let lat_lng = LatLng::from(CellID(cell_id));
    (lat_lng.lat.deg(), lat_lng.lng.deg())
}

pub fn s2_cell_id_to_neighbor_cell_ids(cell_id: u64, edge: bool, corner: bool) -> Option<Vec<u64>> {
    let cell_id = CellID(cell_id);

    if !edge && !corner {
        return None;
    }

    if edge && corner {
        let vertices: Vec<CellID> = cell_id.all_neighbors(cell_id.level());
        let vertices = vertices
            .iter()
            .map(|cell_id| cell_id.0)
            .collect::<Vec<u64>>();
        Some(vertices)
    } else if edge && !corner {
        let vertices = cell_id.vertex_neighbors(cell_id.level());
        let vertices = vertices
            .iter()
            .map(|cell_id| cell_id.0)
            .collect::<Vec<u64>>();
        Some(vertices)
    } else {
        let all_neigbours = cell_id.all_neighbors(cell_id.level());
        let edge_neigbours = cell_id.vertex_neighbors(cell_id.level());
        let all_neigbours = all_neigbours
            .iter()
            .map(|cell_id| cell_id.0)
            .collect::<Vec<u64>>();
        let edge_neigbours = edge_neigbours
            .iter()
            .map(|cell_id| cell_id.0)
            .collect::<Vec<u64>>();
        let all_neigbours: HashSet<u64> = HashSet::from_iter(all_neigbours.iter().cloned());
        let edge_neigbours: HashSet<u64> = HashSet::from_iter(edge_neigbours.iter().cloned());
        let res = all_neigbours
            .difference(&edge_neigbours)
            .copied()
            .collect::<Vec<u64>>();
        Some(res)
    }
}

#[cfg(test)]
mod s2_tests {
    use super::*;

    #[test]
    fn correct_get_s2_cell_id() {
        assert_eq!(
            s2_lat_lon_to_cell_id_one_point(55.96197598047027, 37.35753533375114, 17),
            5095047777373650944
        );
    }

    #[test]
    fn correct_get_lat_lon_from_s2_cell() {
        assert_eq!(
            s2_cell_id_to_lat_lon_one_point(5095047777373650944),
            (55.96197598047027, 37.35753533375114)
        );
    }

    #[test]
    #[ignore]
    fn correct_work_cell_id_to_neighbor_cell_ids() {
        struct CellIdToNeigborTest {
            cell_id: u64,
            edge: bool,
            corner: bool,
            is_none_return_type: bool,
        }

        let inputs = vec![
            CellIdToNeigborTest {
                cell_id: 5095047775628820480,
                edge: false,
                corner: false,
                is_none_return_type: true,
            },
            CellIdToNeigborTest {
                cell_id: 5095047775628820480,
                edge: true,
                corner: false,
                is_none_return_type: false,
            },
            CellIdToNeigborTest {
                cell_id: 5095047775628820480,
                edge: false,
                corner: true,
                is_none_return_type: false,
            },
            CellIdToNeigborTest {
                cell_id: 5095047775628820480,
                edge: true,
                corner: true,
                is_none_return_type: false,
            },
        ];

        let outputs = vec![
            None,
            Some(vec![
                5095047775628820480u64,
                5095047777239433216u64,
                5095047777507868672u64,
                5095047778044739584u64,
            ]),
            Some(vec![
                5095047775763038208u64,
                5095047777105215488u64,
                5095047777642086400u64,
                5095047778178957312u64,
            ]),
            Some(vec![
                5095047775628820480u64,
                5095047775763038208u64,
                5095047777239433216u64,
                5095047777105215488u64,
                5095047777507868672u64,
                5095047777642086400u64,
                5095047778044739584u64,
                5095047778178957312u64,
            ]),
        ];

        for (idx, (input, output)) in std::iter::zip(inputs, outputs).enumerate() {
            println!(
                "case: {}, return type is none: {}",
                idx, input.is_none_return_type
            );

            if input.is_none_return_type {
                assert!(
                    s2_cell_id_to_neighbor_cell_ids(input.cell_id, input.edge, input.corner)
                        .is_none()
                );
            } else {
                let neighbors =
                    s2_cell_id_to_neighbor_cell_ids(input.cell_id, input.edge, input.corner);
                assert!(neighbors.is_some());
                let mut neighbors: Vec<u64> = neighbors.unwrap();
                let mut output = output.unwrap();
                neighbors.sort();
                output.sort();
                println!("neighbors: {:?}, output: {:?}", neighbors, output);
                assert_eq!(neighbors.len(), output.len());
                assert_eq!(neighbors, output);
            }
        }
    }
}
