#![allow(clippy::unused_unit)]

use polars::prelude::*;
use proj::Proj;
use pyo3_polars::derive::polars_expr;
use serde::Deserialize;

#[derive(Deserialize)]
struct TransformKwargs {
    from: String,
    to: String,
    column_names: (String, String),
}

impl Default for TransformKwargs {
    fn default() -> Self {
        TransformKwargs {
            from: "ESPG:4326".to_string(),
            to: "ESPG:3857".to_string(),
            column_names: ("x".to_string(), "y".to_string()),
        }
    }
}

fn transform_func_output_type_with_kwargs(
    _: &[Field],
    kwargs: TransformKwargs,
) -> PolarsResult<Field> {
    let v = vec![
        Field::new(&kwargs.column_names.0, DataType::Float64),
        Field::new(&kwargs.column_names.1, DataType::Float64),
    ];
    Ok(Field::new("coords", DataType::Struct(v)))
}

#[polars_expr(output_type_func_with_kwargs=transform_func_output_type_with_kwargs)]
fn transform(inputs: &[Series], kwargs: TransformKwargs) -> PolarsResult<Series> {
    let coords_ca = inputs[0].struct_()?;

    let x_coord_ser = &coords_ca.fields()[0];
    let y_coord_ser = &coords_ca.fields()[1];

    let x_coord_ser = match x_coord_ser.dtype() {
        DataType::Float32 => x_coord_ser.cast(&DataType::Float64)?,
        DataType::Float64 => x_coord_ser.clone(),
        _ => {
            polars_bail!(InvalidOperation:"Expected coords type struct get to fields with float types")
        }
    };

    let y_coord_ser = match y_coord_ser.dtype() {
        DataType::Float32 => y_coord_ser.cast(&DataType::Float64)?,
        DataType::Float64 => y_coord_ser.clone(),
        _ => {
            polars_bail!(InvalidOperation:"Expected coords type struct get to fields with float types")
        }
    };

    let x_coord_ca = x_coord_ser.f64()?;
    let y_coord_ca = y_coord_ser.f64()?;

    let mut x_coord_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new(&kwargs.column_names.0, x_coord_ca.len());
    let mut y_coord_builder: PrimitiveChunkedBuilder<Float64Type> =
        PrimitiveChunkedBuilder::new(&kwargs.column_names.1, y_coord_ca.len());

    let transformer_res = Proj::new_known_crs(&kwargs.from, &kwargs.to, None);
    if transformer_res.is_err() {
        polars_bail!(InvalidOperation:"Unknown type transform coordinate from: {} or to: {}", &kwargs.from, &kwargs.to);
    }

    let transformer = transformer_res.unwrap();

    x_coord_ca
        .into_iter()
        .zip(y_coord_ca.into_iter())
        .for_each(|(x_opt, y_opt)| match (x_opt, y_opt) {
            (Some(x), Some(y)) => {
                let transformed_coords = transformer.convert((x, y)).unwrap();

                x_coord_builder.append_value(transformed_coords.0);
                y_coord_builder.append_value(transformed_coords.1);
            }
            _ => {
                x_coord_builder.append_null();
                y_coord_builder.append_null();
            }
        });

    let out_ca = StructChunked::new(
        "coords",
        &[
            x_coord_builder.finish().into_series(),
            y_coord_builder.finish().into_series(),
        ],
    )?;

    Ok(out_ca.into_series())
}
