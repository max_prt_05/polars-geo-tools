import polars as pl
import pytest


@pytest.fixture()
def test_df_with_lats_and_lons():
    return pl.DataFrame(
        [
            pl.Series(
                "lat_lon",
                [
                    {
                        "lat": 55.751244,
                        "lon": 37.618423,
                    },
                    {
                        "lat": 59.937500,
                        "lon": 30.308611,
                    },
                    {
                        "lat": 54.715424,
                        "lon": 20.509207,
                    },
                ],
            )
        ]
    )
