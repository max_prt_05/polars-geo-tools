from h3 import h3

import polars_geo_tools as pgs


def test_h3_cell_id_work_correct(test_df_with_lats_and_lons):
    for h3_resolution in range(1, 16):
        h3_cell_id = list(
            test_df_with_lats_and_lons.select(
                pgs.h3.lat_lon_to_cell_id("lat_lon", resolution=h3_resolution).alias(
                    "h3_cell_id"
                )
            )["h3_cell_id"]
        )
        expected_h3_cell_id = [
            int(
                h3.geo_to_h3(
                    row["lat_lon"]["lat"], row["lat_lon"]["lon"], h3_resolution
                ),
                16,
            )
            for row in test_df_with_lats_and_lons.iter_rows(named=True)
        ]
        assert h3_cell_id == expected_h3_cell_id
