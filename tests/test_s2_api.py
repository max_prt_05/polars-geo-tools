from __future__ import annotations

import pytest
import s2cell

import polars_geo_tools as pgs


def test_s2_cell_correct_get(test_df_with_lats_and_lons) -> None:
    for s2_level in range(1, 31):
        s2_cell_id = list(
            test_df_with_lats_and_lons.select(
                pgs.s2.lat_lon_to_cell_id("lat_lon", level=s2_level).alias("s2_cell_id")
            )["s2_cell_id"]
        )
        expected_s2_cell_id = [
            s2cell.lat_lon_to_cell_id(
                row["lat_lon"]["lat"], row["lat_lon"]["lon"], s2_level
            )
            for row in test_df_with_lats_and_lons.iter_rows(named=True)
        ]
        assert s2_cell_id == expected_s2_cell_id


def test_s2_lat_lon_to_cell_id_raise_exception_if_level_incorrect(
    test_df_with_lats_and_lons,
):
    invalid_levels = (1.5, None, -1, 34)
    for invalid_level in invalid_levels:
        with pytest.raises(ValueError):
            test_df_with_lats_and_lons.select(
                pgs.s2.lat_lon_to_cell_id("lat_lon", level=invalid_level)
            )


# def test_s2_cell_id_to_lat_lon()
