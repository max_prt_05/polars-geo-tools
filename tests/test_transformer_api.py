import polars as pl
import pytest
from pyproj import Transformer

import polars_geo_tools as pgs


def test_transform_work_correct(test_df_with_lats_and_lons):
    LLA_FRAME = "EPSG:4326"
    ECEF_FRAME = "EPSG:3857"

    transformer = Transformer.from_crs(LLA_FRAME, ECEF_FRAME, always_xy=True)
    df_with_ecef_coords = test_df_with_lats_and_lons.select(
        pgs.transformer.transform(
            "lat_lon", from_crs=LLA_FRAME, to_crs=ECEF_FRAME
        ).alias("ecef_coords")
    )
    x_coords, y_coords = (
        list(df_with_ecef_coords.select(pl.col("ecef_coords").struct.field("x"))["x"]),
        list(df_with_ecef_coords.select(pl.col("ecef_coords").struct.field("y"))["y"]),
    )
    expected_xy_coords = transformer.transform(
        list(
            test_df_with_lats_and_lons.select(
                pl.col("lat_lon").struct.field("lat"),
            )["lat"]
        ),
        list(
            test_df_with_lats_and_lons.select(
                pl.col("lat_lon").struct.field("lon"),
            )["lon"]
        ),
    )
    expected_x_coords = expected_xy_coords[0]
    expected_y_coords = expected_xy_coords[1]
    assert x_coords == pytest.approx(expected_x_coords)
    assert y_coords == pytest.approx(expected_y_coords)
